# Python Week Of Code, Namibia 2019

We learn how to create a simple form in https://gitlab.com/python-week-of-code-namibia-2019/2019-08-25-form-basic.
We want to avoid code duplication
and delagate more of the form creation to Django.

## Task for Instructor

1. Create the file `blog/forms.py` and add

   ```
   from django import forms

   class PostForm(forms.Form):
       title = forms.CharField(
           label='Title',
           max_length=280
       )
       text = forms.CharField(
           label='Text',
           widget=forms.Textarea
       )
   ```

   Django has many [built-in fields](https://docs.djangoproject.com/en/2.2/ref/forms/fields/#built-in-field-classes).
2. Change the function `blog_form` to

   ```
   def blog_form(request):
       if request.POST:
           form = PostForm(request.POST)
           if form.is_valid():
               post = Post(**form.cleaned_data)
               post.save()
               return redirect('blog', title=post.title)
       else:
           form = PostForm()

       return render(
           request,
           'blog/post_form.html',
           {
               "form": form,
           }
       )
   ```

   in [blog/views.py](blog/views.py).
3. Change [blog/templates/blog/post_form.html](blog/templates/blog/post_form.html) to

   ```
   <!DOCTYPE html>
   <html>
   <head>
   <title>My Site - Blog - New Post</title>
   </head>
   <body>
   <article>
   <h1>New Post</h1>

   <form action="." method="post">
       {% csrf_token %}
       {{ form }}
       <input type="submit" value="Submit">
   <form></form>
   </article>
   </body>
   </html>
   ```
4. Open http://localhost:8000/blog/ with your web browser.
5. Try to submit an empty form.
6. Fill the form and click on "Submit".

## Tasks for Learners

1. Add a field in the form to save the published date.
2. Change the view to save the published date.